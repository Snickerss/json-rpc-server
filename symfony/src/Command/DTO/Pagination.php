<?php


namespace App\Command;


use Symfony\Component\Validator\Constraints as Assert;

class Pagination
{
    /**
     * @Assert\NotBlank()
     */
    public $limit;

    /**
     * @Assert\NotBlank()
     */
    public $offset;
}