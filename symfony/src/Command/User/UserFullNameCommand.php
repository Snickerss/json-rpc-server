<?php

namespace App\Command\User;


use App\Command\DTO\User\UserSomeCommandDTO;

class UserFullNameCommand
{
    /**
     * @param UserSomeCommandDTO $fullNameConcatDTO
     * @return string
     */
    public function __invoke(UserSomeCommandDTO $commandDTO)
    {
        return $commandDTO->getFirstName().' '.$commandDTO->getLastName();
    }
}