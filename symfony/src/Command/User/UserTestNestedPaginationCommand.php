<?php

namespace App\Command\User;


use App\Command\DTO\User\UserSomeCommandDTO;

class UserTestNestedPaginationCommand
{
    public function __invoke(UserSomeCommandDTO $commandDTO)
    {
        return 'SomeCommand. Test Limit: '.$commandDTO->getPagination()->limit;
    }
}