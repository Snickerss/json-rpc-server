<?php

namespace App\Controller;

use Datto\JsonRpc\Server;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ImportEndpointAction
{
    private $server;

    public function __construct(Server $server)
    {
        $this->server = $server;
    }

    /**
     * @Route(name="import_json_rpc_endpoint", methods={"POST"})
     */
    public function __invoke(Request $request): Response
    {
        $data = $this->server->reply($request->getContent());

        return new JsonResponse($data, Response::HTTP_OK, [], true);
    }
}