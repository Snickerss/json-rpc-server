<?php


namespace App\JsonRPCServer;


use App\Command\User\UserTestNestedPaginationCommand;
use App\JsonRPCServer\ArgumentResolver\ArgumentResolver;
use App\JsonRPCServer\ArgumentResolver\JsonRPCArgumentResolver\CustomValidatorException;
use App\Command\User\UserFullNameCommand;
use Datto\JsonRpc\Evaluator;
use Datto\JsonRpc\Exceptions\MethodException;
use Psr\Container\ContainerInterface;
use Symfony\Component\Serializer\NameConverter\NameConverterInterface;
use Symfony\Component\Validator\ConstraintViolationInterface;
use Symfony\Contracts\Service\ServiceSubscriberInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class Api implements Evaluator, ServiceSubscriberInterface
{
    /**
     * @var ArgumentResolver
     */
    private $argumentResolver;

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * @var NameConverterInterface
     */
    private $nameConverter;

    public function __construct(
        ArgumentResolver $argumentResolver,
        ContainerInterface $container,
        TranslatorInterface $translator,
        NameConverterInterface $nameConverter
    ) {
        $this->argumentResolver = $argumentResolver;
        $this->container = $container;
        $this->translator = $translator;
        $this->nameConverter = $nameConverter;
    }

    /**
     * @param string $method
     * @param array $arguments
     * @return mixed
     * @throws CustomArgumentException
     * @throws MethodException
     */
    public function evaluate($method, $arguments)
    {
        if (!$this->container->has($method)) {
            throw new MethodException();
        }

        $callable = $this->container->get($method);
        assert(is_callable($callable));

        try {
            $args = $this->argumentResolver->getArguments($arguments, $callable);
        } catch (CustomValidatorException $exception) {
            $errors = array_map(fn (ConstraintViolationInterface $violation) => [
                'field' => $this->nameConverter->normalize($violation->getPropertyPath()),
                'message' => $this->translator->trans(
                    $violation->getMessageTemplate(),
                    $violation->getParameters(),
                    'validators'
                ),
            ], iterator_to_array($exception->violationsList));

            throw new CustomArgumentException($errors);
        }

        return $callable(...$args);
    }

    public static function getSubscribedServices()
    {
        return [
            'User.getFullName' => UserFullNameCommand::class,
            'User.testNestedPagination' => UserTestNestedPaginationCommand::class,
        ];
    }
}