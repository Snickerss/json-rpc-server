<?php


namespace App\JsonRPCServer;

use Datto\JsonRpc\Exceptions\Exception as BaseException;
use Datto\JsonRpc\Responses\ErrorResponse;

class CustomArgumentException extends BaseException
{
    public function __construct($errors = [])
    {
        parent::__construct('Invalid params', ErrorResponse::INVALID_ARGUMENTS, $errors);
    }
}