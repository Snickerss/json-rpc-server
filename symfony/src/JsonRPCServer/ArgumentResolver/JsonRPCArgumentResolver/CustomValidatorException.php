<?php


namespace App\JsonRPCServer\ArgumentResolver\JsonRPCArgumentResolver;


use Symfony\Component\Validator\Exception\ValidatorException;
use Throwable;

class CustomValidatorException extends ValidatorException
{
    public $violationsList;

    public function __construct($errors = null, $message = "", $code = 0, Throwable $previous = null, $formatErrors = [])
    {
        parent::__construct($message, $code, $previous);
        $this->violationsList = $errors;
    }
}