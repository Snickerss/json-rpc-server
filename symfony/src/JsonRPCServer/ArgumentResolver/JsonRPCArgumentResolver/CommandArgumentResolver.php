<?php

namespace App\JsonRPCServer\ArgumentResolver\JsonRPCArgumentResolver;


use App\JsonRPCServer\ArgumentResolver\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class CommandArgumentResolver implements ArgumentValueResolverInterface
{
    /**
     * @var DenormalizerInterface
     */
    private $denormalizer;

    /**
     * @var ValidatorInterface
     */
    private $validator;

    public function __construct(DenormalizerInterface $denormalizer, ValidatorInterface $validator)
    {
        $this->denormalizer = $denormalizer;
        $this->validator = $validator;
    }

    /**
     * @param $data
     * @param ArgumentMetadata $argument
     * @return bool
     */
    public function supports($data, ArgumentMetadata $argument)
    {
        return $argument->getName() == 'commandDTO';
    }

    /**
     * @param $data
     * @param ArgumentMetadata $argument
     * @return iterable|void
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    public function resolve($data, ArgumentMetadata $argument)
    {
        $class = $argument->getType();
        assert(class_exists($class));

        $context = [
            'disable_type_enforcement' => true,
            'allow_extra_attributes' => true
        ];

        $dto = $this->denormalizer->denormalize($data, $class, null, $context);

        $errors = $this->validator->validate($dto);

        if ($errors->count() !== 0) {
            throw new CustomValidatorException($errors);
        }

        yield $dto;
    }
}