<?php


namespace App\JsonRPCServer\ArgumentResolver;


interface ArgumentResolverInterface
{
    public function getArguments($request, callable $controller);
}